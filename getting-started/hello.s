# -*- mode: asm; lexical-binding: t -*-
# ========================================================================
# $File: hello.s $
# $Date: 2023-12-21 01:49:20 $
# $Revision: $
# $Creator: Jen-Chieh Shen $
# $Notice: See LICENSE.txt for modification and distribution information
#                   Copyright © 2023 by Shen, Jen-Chieh $
# ========================================================================

# Input Functions (extern)

# Output Functions (global)
.global _start

# `bss' is for the uninitialized data in RAM which is initialized with zero
# in the startup code.
.bss

# `data' is for initialized variables, and it counts for RAM and FLASH.
# The linker allocates the data in FLASH which then is copied from ROM to
# RAM in the startup code.
.data
message:
    .ascii "Hello, world\n"

# `text' is my code, vector table plus constants.
.text
_start:
    # write(1, message, 13)
    mov     $1, %rax                # system call 1 is write
    mov     $1, %rdi                # file handle 1 is stdout
    mov     $message, %rsi          # address of string to output
    mov     $13, %rdx               # number of bytes
    syscall                         # invoke operating system to do the write

    # exit(0)
    mov     $60, %rax               # system call 60 is exit
    xor     %rdi, %rdi              # we want return code 0
    syscall                         # invoke operating system to exit
