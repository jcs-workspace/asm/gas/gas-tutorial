#!/bin/bash
# ========================================================================
# $File: setup.sh $
# $Date: 2023-12-21 01:47:58 $
# $Revision: $
# $Creator: Jen-Chieh Shen $
# $Notice: See LICENSE.txt for modification and distribution information
#                   Copyright © 2023 by Shen, Jen-Chieh $
# ========================================================================

apt-get update
apt-get install git
apt-get install make
apt-get install gcc
apt-get install nasm
apt-get install binutils   # For ld
